package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

const BUFFER_SIZE = 1024 * 1024

var myArgs []string
var myArgsIdx []int
var myQueue chan string = make(chan string, BUFFER_SIZE)
var queueDone chan byte = make(chan byte)

func qRunner() {
	for qItem := range myQueue {
		for _, x := range myArgsIdx {
			myArgs[x] = qItem
		}
		fmt.Printf("Running: %v\n", myArgs)
		cmd := exec.Command(myArgs[0], myArgs[1:]...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Run()
	}
	queueDone <- 1
}

func main() {
	if len(os.Args) < 3 {
		fmt.Printf("Usage: %v program {}\n", os.Args[0])
		return
	}
	// support for find -print0
	delim := byte('\n')
	_, nullMode := os.LookupEnv("NULL_MODE")
	if nullMode {
		delim = byte(0)
	}
	str_delim := string(delim)
	myArgs = os.Args[1:]
	fmt.Printf("Args: %v\n", myArgs)
	for idx, elem := range myArgs {
		if elem == "{}" {
			myArgsIdx = append(myArgsIdx, idx)
		}
	}
	if len(myArgsIdx) < 1 {
		fmt.Println("WARNING: You haven't included any {} in the arugment list")
	}
	go qRunner()
	stdin := bufio.NewReader(os.Stdin)
	for {
		in, err := stdin.ReadString(delim)
		if err != nil {
			break
		}
		text := strings.TrimSuffix(in, str_delim)
		myQueue <- text
		fmt.Printf("Added: %v\n", text)
	}
	fmt.Println("EOF Reached")
	close(myQueue)
	<-queueDone
	return
}
