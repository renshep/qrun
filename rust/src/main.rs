use std::process::Command;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};

fn main() {
    let mut args = vec![];
    let mut arg_idx = vec![];
    let mut bin_name = String::from("qrun");
    let mut curarg_idx = 0;
    let mut first_arg = true;
    let (tx, rx): (Sender<String>, Receiver<String>) = mpsc::channel();

    for arg in std::env::args() {
        if first_arg {
            first_arg = false;
            bin_name = arg;
            continue;
        } else {
            if arg == "{}" {
                arg_idx.push(curarg_idx);
            }
            args.push(arg);
        }
        curarg_idx += 1;
    }
    if args.len() < 1 {
        println!("Usage: {} program {{}}", bin_name);
        return;
    }
    if arg_idx.len() < 1 {
        println!("WARNING: You haven't included any {{}} in the argument list");
    }
    println!("args: {:?}", args);
    // qrunner thread
    let child = std::thread::spawn(move || loop {
        let text_in = rx.recv().unwrap();
        if text_in == "" {
            println!("Queue Finished");
            break;
        }
        for x in arg_idx.iter() {
            args[x.to_owned()] = text_in.clone();
        }
        let mut qarg_command = args.clone();
        let args_list = qarg_command.split_off(1);
        println!("Running: {:?} with args {:?}", qarg_command[0], args_list);
        let mut cmd = Command::new(&qarg_command[0]);
        let _cmd_err = cmd.args(args_list).spawn().unwrap().wait();
    });
    // handle standard input
    let stdin = std::io::stdin();
    loop {
        let mut text = String::new();
        let _err = stdin.read_line(&mut text);
        if text == "\n" {
            continue;
        }
        if text == "" {
            let _err = tx.send(String::new());
            break;
        }
        text.pop(); // remove trailing new line
        println!("Added: {}", text);
        let _err = tx.send(text);
    }
    println!("EOF Reached");
    // wait for the queue to finish
    let _res = child.join();
}
